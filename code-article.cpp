#include "DGtal/base/Common.h"
#include "DGtal/helpers/StdDefs.h"
#include <DGtal/kernel/domains/HyperRectDomain.h>
#include <DGtal/kernel/SpaceND.h>
#include "DGtal/io/boards/Board2D.h"
#include "DGtal/io/colormaps/GrayscaleColorMap.h"
#include "DGtal/images/ImageContainerBySTLVector.h"
#include "DGtal/images/ConstImageAdapter.h"
#include "DGtal/io/readers/PGMReader.h"
#include "DGtal/io/writers/PGMWriter.h"

#include "DGtal/base/IteratorCirculatorTraits.h"
#include "DGtal/geometry/tools/Hull2DHelpers.h"
#include "DGtal/geometry/tools/PolarPointComparatorBy2x2DetComputer.h"
#include "DGtal/geometry/tools/determinant/AvnaimEtAl2x2DetSignComputer.h"
#include "DGtal/geometry/tools/determinant/InHalfPlaneBySimple3x3Matrix.h"
#include "DGtal/shapes/ShapeFactory.h"
#include "DGtal/shapes/Shapes.h"
#include "DGtal/topology/DigitalSetBoundary.h"
#include "DGtal/topology/DigitalSurface.h"
#include "DGtal/graph/DepthFirstVisitor.h"

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <iomanip>

#define INIT_LABEL 1000
#define NUMBER_NODE_MAX 1000
#define PI 3.14159265
#define DELTA 0.001

using namespace std;
using namespace DGtal;
using namespace DGtal::Z2i;

typedef struct 
{
	double x;
	double y;
	bool considered = true;
}DoublePoint;

typedef struct 
{
	Point coordinate;
	int valuePixel;
	bool isComponent;
	int label;
	int countRepeat = 0;
}MyPoint;

typedef struct 
{
	int id;
	int labelParent;
	int level;
	int originalColor;

	vector<Point> points;
	vector<Point> hull;
	vector<Point> boundery8;
	vector<Point> polygon;
	
	Color color;
	
	bool isHConvex = false;
	bool isSubComponent = false;

	int nbPointBoundary8 = 0;
	int nbPointBoundary4 = 0;
	int nbPixelInHull = 0;
	int indexBreakPointHull = -1;
	
	double perimeter = 0;
	double area = 0;
}Region;

struct Node
{
	int idRegion;
	int idParents;
	int levelInTree = -1;
	int idNode;
	vector<Node> subnodes;
	bool isRoot = true;
};

typedef struct Node Node;

typedef struct 
{
	int idRootRegion;
	int idRoot;
	vector<Node> nodes;
}Tree;

typedef ImageContainerBySTLVector<Domain, unsigned char> Image;
typedef ConstImageAdapter<Image, Domain, functors::Identity, bool, DGtal::functors::Thresholder<Image::Value> > ConstImageAdapterForThresholderImage;

//==================== Declaration functions ====================

void getConcaveTree(Board2D &aBoard, Domain domainImage, int idCurrentRegion, vector<Region> &regions, int &levelRegion, vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, Tree &tree, int valuePixelComponent, ConstImageAdapterForThresholderImage &thresholderImage);
void get4ConnectivityRegion(Domain domain, vector<vector<MyPoint>> &myPoints, vector<Region> &regions, int &levelRegion, int numberPixelEnlarge);
void mergeTwoVector(vector<int> &v1, vector<int> &v2);
void mergeTwoEquivalentRegion(vector<vector<int>> &equivalentRegions, int label1, int label2, int countRegion);
void initMyPoint(Domain domain, vector<vector<MyPoint>> &myPoints, int deltaX, int deltaY, int initLabel);
void drawHull(Board2D &aBoard, vector<Point> &hull);
void getConvexHull(Region &component);
void sortTwoDoublePoint(DoublePoint &p1, DoublePoint &p2);
void mergeEquivalentRegion(vector<vector<int>> &equivalentRegions, int sizeEquivalentRegion, vector<Region> &regions, vector<vector<MyPoint>> &myPoints, Domain domain, int &levelRegion, int countRegion, int numberPixelEnlarge);
void formWellComposed(Image &image, int thresholdImage);
void coloringConnectedComponent(Board2D &aBoard, Region region);
void initTrees(vector<Region> &regions, vector<Tree> &trees);
void generateColorImage(Board2D &aBoard, vector<Region> &regions);
void generateGraphFile(vector<Tree> &trees, vector<Region> &regions);
void generateTextImage(vector<Region> &regions, Domain domain, int numberPixelEnlarge);
void initValueVector(vector<int> &v, int value, int size);
void getAdjacencyTree(Domain domain, vector<Region> &regions, Tree &tree, int numberPixelEnlarge, vector<vector<MyPoint>> &myPoints);
void intersectionVectorV1ToVectorV2(vector<int> &v1, vector<int> & v2);
void removeElementByValueFromVector(vector<int> &v, int value);
void intersectionTwoVectors(vector<int> &v1, vector<int> &v2);
void getPointsBackward(int &nbPixelInHull, vector<Point> &points, Point point, int label, double angle, int dx, int dy, vector<vector<MyPoint>> &myPointsTransformationPointByPoint, int deltaX, int deltaY);
void rotation(DoublePoint input, DoublePoint &output, double angle);
void transformation(DoublePoint input, DoublePoint &output, int dx, int dy);
void tracePointInLine(vector<Point> &points, DoublePoint p1, DoublePoint p2, vector<vector<MyPoint>> &myPoints, int deltaX, int deltaY, int label);
void getPointInDoubleHull(vector<Point> &points, vector<DoublePoint> &hull, vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY, int label);
void getPointInLine(vector<Point> &points, double leftPoint_X, double leftPoint_Y, double rightPoint_X, double rightPoint_Y);
void getPointInFlatTopTriangle(vector<Point> &points, DoublePoint p1, DoublePoint p2, DoublePoint p3);
void getPointInFlatBottomTriangle(vector<Point> &points, DoublePoint p1, DoublePoint p2, DoublePoint p3);
void getPointInFullTriangle(vector<Point> &points, DoublePoint p1c, DoublePoint p2c, DoublePoint p3c, vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY, int label);
void getTransformationDomain(Point p1, Point p2, Point p3, Point p4, Point &p5, Point &p6, double angle, int dx, int dy);
void calculateNbPixelInHullOfOriginalImage(Tree tree, vector<Region> &regions);
void calculateComplementaryInformation(vector<Point> &hull, double &perimeter, double &area);
void getBoundery(vector<Point> &hull, vector<vector<MyPoint>> &myPoints, int &nbPointBoundary8, int &nbPointBoundary4, int labelCurrent, int labelParent, int deltaX, int deltaY);
void traceBoundery(Point startPoint, vector<vector<MyPoint>> &myPoints, int &nbPointBoundary8, int &countNotWellComposed, int labelCurrent, int labelParent, int deltaX, int deltaY);
void initCountRepeat(vector<vector<MyPoint>> &myPointsTransformationPointByPoint);
void drawBoundery(Board2D &aBoard, vector<Point> &boundery);
void drawPolygon(Board2D &aBoard, vector<Point> &boundery);
void getBoundery8(vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, Region &connectedComponent);
void getPolygon(vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, Region &connectedComponent);
void sortPoint(Point &startPoint, Point &endPoint);
void generatePolygonFile(vector<Region> &regions);
void getConcavePolygonTree(Region region, Tree &tree, vector<Region> &regions);
void getPolygonInConcavePart(vector<vector<MyPoint>> &myPoints, Region &connectedComponent, Point startPoint, Point endPoint, int numberPixelEnlarge);
void sortTwoVector(vector<Point> &vector1, vector<Point> &vector2);

bool isConcavePart(vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, Point startPoint, Point endPoint, bool onHullUp);
bool testLinePolygon(vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, Point startPoint, Point endPoint, vector<Point> &boundery8, int startIndexBoundery, int endIndexBoundery);
bool isNotWellComposedInBoundery(Point p1, Point p2);
bool testLine(double leftPoint_X, double leftPoint_Y, double rightPoint_X, double rightPoint_Y, int idRegion, vector<Point> &complementaryPoints, vector<vector<MyPoint>> &myPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge);
bool testFlatTopTriangle(DoublePoint p1, DoublePoint p2, DoublePoint p3, int idRegion, vector<Point> &complementaryPoints, vector<vector<MyPoint>> &myPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge);
bool testFlatBottomTriangle(DoublePoint p1, DoublePoint p2, DoublePoint p3, int idRegion, vector<Point> &complementaryPoints, vector<vector<MyPoint>> &myPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge);
bool testFullTriangle(Point p1, Point p2, Point p3, int idRegion, vector<Point> &complementaryPoints, vector<vector<MyPoint>> &myPoints, int &maxX, int &maxY, int &minX, int &minY,int numberPixelEnlarge);
bool checkHConvex(Board2D &aBoard, int idRegion, vector<Point> &hull, vector<vector<MyPoint>> &myPoints, vector<Point> &complementaryPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge);

Point getPointTopLeft(vector<Point> &points);
Point getNextBounderyPoint(Point topLeft, int &dir, Point currentPoint, vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge);

vector<Region> transformationPointByHull(vector<Region> &regions, vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY, double angle, int dx, int dy);
vector<Region> transformationPointByPoint(vector<Region> &regions, double angle, int dx, int dy, vector<vector<MyPoint>> &myPointsTransformationPointByPoint, int deltaX, int deltaY);
vector<Region> transformationByConcavePolygonTree(vector<Region> &regions, 
	vector<vector<MyPoint>> &myPointsTransformation, int deltaX, int deltaY,
 	double angle, int dx, int dy, Tree tree);

DoublePoint rigidMotion(DoublePoint input, double angle, int dx, int dy);

int  getMinInteger(int a, int b);
int  getMaxInteger(int a, int b);
int  findIdEquivalentRegion(vector<vector<int>> regions, int labelCurrent, int numberRegion);
int  getIndexByValue(vector<Point> points, Point value);

double calculateDistance(Point p1, Point p2);
double calculateAreaTriangle(Point p1, Point p2, Point p3);
double cross(const Point &p0, const Point &p1, const Point &p2);
double getMinDouble(double a, double b);
double getMaxDouble(double a, double b);

//==============================================================//
//==============================================================//
//====================== End declaration =======================//
//======================== Begin main ==========================//
//==============================================================//
//==============================================================//

int main( int argc, char** argv)
{
	if (argc < 10)
	{
	    cout << "Usage : ./code-article nameImage threshold number_pixel_enlarge type_pixel_enlarge angleStart angleEnd deltaAngle dx dy\n";
	    return 1;
	}
	  
	//Get parameters from command line
	stringstream ss;
	ss << argv[1];
	int threshold = atoi(argv[2]);
	int numberPixelEnlarge = atoi(argv[3]);
	int typePixelEnlarge = atoi(argv[4]);
	int angleStart = atoi(argv[5]);
	int angleEnd = atoi(argv[6]);
	int deltaAngle = atoi(argv[7]);
	int dx = atoi(argv[8]);
	int dy = atoi(argv[9]);

	//Read image
	string nameImage;
	ss >> nameImage;
	string filename1 = "samples/"+nameImage;
	Image image = DGtal::PGMReader<Image>::importPGM(filename1); 

	//Get width and height of image
	Image::Domain domain = image.domain();
	int widthImg = domain.upperBound()[0];
	int heightImg = domain.upperBound()[1];

	//Create a domain to expand image
	Point p1(-numberPixelEnlarge, -numberPixelEnlarge), p2(widthImg + numberPixelEnlarge, heightImg + numberPixelEnlarge);
	Image::Domain expandedDomain(p1, p2);

	//Remove pattern not well-composed in image
	formWellComposed(image, threshold);
	  
	//Create binary image
	DGtal::functors::Thresholder<Image::Value> t(threshold);
	functors::Identity df;  
	ConstImageAdapterForThresholderImage thresholderImage(image, expandedDomain, df, t);
	thresholderImage.setDefaultValue(typePixelEnlarge);

	//Save binary image
	typedef GrayscaleColorMap<unsigned char> Gray;  
	Board2D aBoard;
	aBoard.clear();    
	Display2DFactory::drawImage<Gray>(aBoard, thresholderImage, (unsigned char)0, (unsigned char)1); 
	aBoard.saveSVG("result/thresholderImage.svg");
	aBoard << SetMode( expandedDomain.className(), "Paving" ) << expandedDomain;

	Domain domainThresholderImage = thresholderImage.domain();
	int width = domainThresholderImage.upperBound()[0] - domainThresholderImage.lowerBound()[0];
	int height = domainThresholderImage.upperBound()[1] - domainThresholderImage.lowerBound()[1];

	
	//Init matrix myPoints
	MyPoint p;
	vector<vector<MyPoint>> myPoints(width + 1, vector<MyPoint>(1, p ));

	initMyPoint(domainThresholderImage, myPoints, -numberPixelEnlarge, -numberPixelEnlarge, INIT_LABEL);

	for (int i = 0; i < myPoints.size(); ++i)
	{
		for(int j = 0; j < myPoints.at(0).size(); j++)
		{
			myPoints[i][j].valuePixel = (int)thresholderImage(myPoints[i][j].coordinate);
		}
	}

	//Get connected component
	vector<Region> regions;
	int levelRegion = 0;
	get4ConnectivityRegion(domainThresholderImage, myPoints, regions, levelRegion, numberPixelEnlarge);
	cout << "4connec" << endl;

	aBoard.clear(); 
	generateColorImage(aBoard, regions);
	aBoard.saveSVG("result/colorImage.svg");

	//Get tree region
	Tree tree;
	tree.idRootRegion = -1;
	for (int i = 0; i < regions.size(); ++i)
	{
		Node node;
		node.idRegion = regions.at(i).id;
		tree.nodes.push_back(node);
	}

	getAdjacencyTree(domainThresholderImage, regions, tree, numberPixelEnlarge, myPoints);
	cout << "adjtree" << endl;

	for(int i = 0; i < tree.nodes.size(); i++)
	{
	    if (tree.nodes.at(i).isRoot)
	    {
	      tree.idRootRegion = tree.nodes.at(i).idRegion;
	      break;
	    }
	}

	vector<Tree> trees;
	trees.push_back(tree);
	generateGraphFile(trees, regions);

	//Get connected-component exclusion background
	vector<Region> connectedComponents;
	for(int i = 0; i < regions.size(); i++)
	{
		if(regions.at(i).id != tree.idRootRegion)
		{
			connectedComponents.push_back(regions.at(i));
		}
	}

	aBoard.clear(); 
	aBoard << SetMode( expandedDomain.className(), "Paving" ) << expandedDomain;
	generateColorImage(aBoard, connectedComponents);

	for(int i = 0; i < connectedComponents.size(); i++)
	{
		getBoundery8(myPoints, numberPixelEnlarge, connectedComponents.at(i));
		getPolygon(myPoints, numberPixelEnlarge, connectedComponents.at(i));
		//drawBoundery(aBoard, connectedComponents.at(i).boundery8);
		drawPolygon(aBoard, connectedComponents.at(i).polygon);
	}

	generatePolygonFile(connectedComponents);

	aBoard.saveSVG("result/polygon.svg");

	Tree concavePolygonTree;
	cout << "size polygon " << connectedComponents.at(0).polygon.size() << endl;
	getConcavePolygonTree(connectedComponents.at(0), concavePolygonTree, regions);

	for(int i = 0; i < concavePolygonTree.nodes.size(); i++)
	{
	    if (concavePolygonTree.nodes.at(i).isRoot)
	    {
	      concavePolygonTree.idRootRegion = concavePolygonTree.nodes.at(i).idRegion;
	      concavePolygonTree.idRoot = i;
	      concavePolygonTree.nodes.at(i).levelInTree = 0;
	      break;
	    }
	}

	vector<Node> nodes = concavePolygonTree.nodes;
	for(int i = 0; i < nodes.size(); i++){
		for(int j = 1; j < nodes.size(); j++){
			if(nodes.at(j).idParents == i){
				concavePolygonTree.nodes.at(i).subnodes.push_back(concavePolygonTree.nodes.at(j));
			}
		}
	}

	stack<Node> stackNodes;
	stackNodes.push(concavePolygonTree.nodes.at(concavePolygonTree.idRoot));

	while(!stackNodes.empty())
	{
		Node currentNode = stackNodes.top();
		stackNodes.pop();
		vector<Node> subnodes = currentNode.subnodes;
		for(int i = 0; i < subnodes.size(); i++){
			if(subnodes.at(i).levelInTree == -1){
				concavePolygonTree.nodes.at(subnodes.at(i).idNode).levelInTree = currentNode.levelInTree + 1;
			}
			stackNodes.push(concavePolygonTree.nodes.at(subnodes.at(i).idNode));
		}
	}

	aBoard.clear(); 
	aBoard << SetMode( expandedDomain.className(), "Paving" ) << expandedDomain;
	for(int i = 0; i < concavePolygonTree.nodes.size(); i++){
			aBoard.setPenColor(Color(rand() % 192 + 64, rand() % 192 + 64, rand() % 192 + 64, 255));
		    drawHull(aBoard, regions.at(concavePolygonTree.nodes.at(i).idRegion).hull);
	}
	aBoard.saveSVG("result/concavePolygon.svg");


	for(int _angle = angleStart; _angle <= angleEnd; _angle = _angle + deltaAngle)
	{
		cout <<"angle " << _angle << endl;
		double angle = (double)(_angle) * PI / 180;

		//Domain of image transformation
	 	Point p3(widthImg + numberPixelEnlarge, -numberPixelEnlarge), p4(-numberPixelEnlarge, heightImg + numberPixelEnlarge);
	 	Point p5, p6;
	 	getTransformationDomain(p1, p2, p3, p4, p5, p6, angle, dx, dy);
		Image::Domain transformationDomain(p5, p6);

		int widthTransformationImage = transformationDomain.upperBound()[0] - transformationDomain.lowerBound()[0];
		int heightTransformationImage = transformationDomain.upperBound()[1] - transformationDomain.lowerBound()[1];
		int deltaX = transformationDomain.lowerBound()[0];
		int deltaY = transformationDomain.lowerBound()[1];

		MyPoint _p;
		vector<vector<MyPoint>> myPointsTransformation(widthTransformationImage + 1, vector<MyPoint>(1, _p ));
		initMyPoint(transformationDomain, myPointsTransformation, deltaX, deltaY, tree.idRootRegion);
		vector<Region> regionsTransformation = 
			transformationByConcavePolygonTree(regions, myPointsTransformation, deltaX, deltaY, 
				angle, dx, dy, concavePolygonTree);
	
		aBoard.clear();
		aBoard << SetMode(transformationDomain.className(), "Paving" ) << transformationDomain;
		generateColorImage(aBoard, regionsTransformation);
		stringstream ss;
		ss << _angle;
		string strAngle = ss.str();
		string nameFile = "result/transformationImage" + strAngle + ".svg";
		aBoard.saveSVG(nameFile.c_str());
	}

	return 0;
}

//==============================================================//
//==============================================================//
//==================== Definition functions ====================//
//==============================================================//
//==============================================================//

vector<Region> transformationByConcavePolygonTree(vector<Region> &regions, 
	vector<vector<MyPoint>> &myPointsTransformation, int deltaX, int deltaY,
 	double angle, int dx, int dy, Tree tree)
{
	vector<Region> regionsTransformation;

	for(int i = 0; i < tree.nodes.size(); i++){
		Node currentNode = tree.nodes.at(i);
		vector<Point> hull = regions.at(currentNode.idRegion).hull;

		cout << "idNode = " << currentNode.idNode << endl;
		cout << "levelInTree = " << currentNode.levelInTree << endl;
		cout << "idRegion = " << currentNode.idRegion << endl;
		cout << endl << "hull" << endl;
		for(int j = 0; j < hull.size(); j++){
			cout << hull.at(j) << endl;
		}

		cout << "========================================" << endl;

		Region r;
		r = regions.at(currentNode.idRegion);
		r.points.clear();
		r.hull.clear();

		vector<DoublePoint> hullTransformation;
		for(int j = 0; j < hull.size(); j++)
		{
			DoublePoint p;
 			p.x = hull.at(j)[0];
 			p.y = hull.at(j)[1];

			p = rigidMotion(p, angle, dx, dy);
			hullTransformation.push_back(p);
		}

		/*if (hullTransformation.size() == 1)
		{
			Point p(hullTransformation.at(0).x, hullTransformation.at(0).y);
			r.points.push_back(p);
			myPointsTransformationByHull[p[0] - deltaX][p[1] - deltaY].label = r.id;
		} else if(hullTransformation.size() == 2)
		{
			tracePointInLine(r.points, hullTransformation.at(0), hullTransformation.at(1), myPointsTransformationByHull, deltaX, deltaY, r.id);
		} else
		{*/
			getPointInDoubleHull(r.points, hullTransformation, myPointsTransformation, deltaX, deltaY, 
				r.id);
		//}
		vector<Point> points = r.points;
		//cout << "test " << currentNode.levelInTree << " " << currentNode.idNode << " " << points.size() << endl;
		/*for(int i = 0; i < points.size(); i++){
			if(currentNode.levelInTree % 2 == 0){
				myPointsTransformation[points.at(i)[0] - deltaX][points.at(i)[1] - deltaY].countRepeat++;
			}else{
				myPointsTransformation[points.at(i)[0] - deltaX][points.at(i)[1] - deltaY].countRepeat--;
			}
			
		}*/

		for(int ii = 0; ii < myPointsTransformation.size(); ii++){
			for(int jj = 0; jj < myPointsTransformation.at(0).size(); jj++){
				if(myPointsTransformation[ii][jj].label == r.id){
					if(currentNode.levelInTree % 2 == 0){
						myPointsTransformation[ii][jj].countRepeat++;
					}else{
						myPointsTransformation[ii][jj].countRepeat--;
					}
				}
			}
		}

		//getConvexHull(r);
		//calculateComplementaryInformation(r.hull, r.perimeter, r.area);
		//r.nbPixelInHull = r.points.size();

		//regionsTransformation.push_back(r);

		for(int ii = 0; ii < myPointsTransformation.size(); ii++){
			for(int jj = 0; jj < myPointsTransformation.at(0).size(); jj++){
				//cout << myPointsTransformation[ii][jj].countRepeat << " ";
			}
			//cout << endl;
		}

		//cout << endl;
		//cout << "----------------------------------------" << endl;
	}

	Region region;

	for(int i = 0; i < myPointsTransformation.size(); i++){
		for(int j = 0; j < myPointsTransformation.at(0).size(); j++){
			//cout << myPointsTransformation[i][j].countRepeat << " ";
			if(myPointsTransformation[i][j].countRepeat == 1){
				region.points.push_back(Point(i + deltaX, j + deltaY));
			}
		}
		//cout << endl;
	}

	region.color = Color(0, 0, 0);

	regionsTransformation.push_back(region);

	return regionsTransformation;
}

void getConcavePolygonTree(Region region, Tree &tree, vector<Region> &regions){
	int indexRegion = regions.size();

	Region r;
	r.points = region.polygon;
	r.id = indexRegion;
	indexRegion++;
	getConvexHull(r);


	int idNode = 0;

	Node node;
	node.idRegion = r.id;
	node.idNode = idNode;
	tree.nodes.push_back(node);

	regions.push_back(r);

	stack<Region> stackPolygon;
	stackPolygon.push(r);
	while(!stackPolygon.empty()){
		Region currentPolygon = stackPolygon.top();
		stackPolygon.pop();

		if(currentPolygon.points.size() > currentPolygon.hull.size()){
			bool polygonIsAntilock = true;
			vector<Point> hull = currentPolygon.hull;
			vector<Point> polygon = currentPolygon.points;
			for(int i = 0; i < hull.size(); i++){
				cout <<"hull " << i << " " << hull.at(i) << endl;
			}
			for(int i = 0; i < polygon.size(); i++){
				cout <<"polygon " << i << " " << polygon.at(i) << endl;
			}

			for(int i = 0; i < hull.size() - 1; i++){
				int index1 = getIndexByValue(polygon, hull.at(i));
				int index2 = getIndexByValue(polygon, hull.at(i + 1));
				if(index1 > index2){
					polygonIsAntilock = false;
				}
				cout << index1 << " " << index2 << endl;

				if(abs(index1 - index2) > 1 && abs(index1 - index2) < polygon.size() - 1){
					Region newPolygon;

					int startIndex = getMinInteger(index1, index2);
					int endIndex = getMaxInteger(index1, index2);

					for(int j = startIndex; j <= endIndex; j++){
						newPolygon.points.push_back(polygon.at(j));
						//cout << "newPolygon " << newPolygon.points.at(newPolygon.points.size() - 1) << endl;
					}


					newPolygon.id = indexRegion;
					getConvexHull(newPolygon);
					indexRegion++;
					for(int k = 0; k < newPolygon.hull.size(); k++){
						//cout << "new hull " << newPolygon.hull.at(k) << endl;
					}
						
					idNode++;
					Node node;
					node.idRegion = newPolygon.id;
					node.isRoot = false;
					node.idNode = idNode;
					for(int k = 0; k < tree.nodes.size(); k++){
						if(tree.nodes.at(k).idRegion == currentPolygon.id){
							node.idParents = tree.nodes.at(k).idNode;
							break;
						}
					}
					cout << "IDs " << node.idParents << " " << node.idNode << " " << node.idRegion << endl;
					cout << "----------------end---------------" << endl;
						
					tree.nodes.push_back(node);
					stackPolygon.push(newPolygon);
					regions.push_back(newPolygon);
				}
			}

			int index1 = getIndexByValue(polygon, hull.at(0));
			int index2 = getIndexByValue(polygon, hull.at(hull.size() - 1));
			cout <<"END " << index1 << " " << index2 << endl;

			if(abs(index1 - index2) > 1 && abs(index1 - index2) < polygon.size() - 1){
				Region newPolygon;

				int minIndex = getMinInteger(index1, index2);
				int maxIndex = getMaxInteger(index1, index2);

				if(polygonIsAntilock){
					for(int j = maxIndex; j < polygon.size(); j++){
						newPolygon.points.push_back(polygon.at(j));
						//cout << "newPolygon " << newPolygon.points.at(newPolygon.points.size() - 1) << endl;
					}
					for(int j = 0; j <= minIndex; j++){
						newPolygon.points.push_back(polygon.at(j));
						//cout << "newPolygon " << newPolygon.points.at(newPolygon.points.size() - 1) << endl;
					}
				}else{
					for(int j = minIndex; j <= maxIndex; j++){
						newPolygon.points.push_back(polygon.at(j));
						//cout << "newPolygon " << newPolygon.points.at(newPolygon.points.size() - 1) << endl;
					}
				}

				newPolygon.id = indexRegion;
				getConvexHull(newPolygon);
				indexRegion++;
				for(int k = 0; k < newPolygon.hull.size(); k++){
					//cout << "new hull " << newPolygon.hull.at(k) << endl;
				}
						
				idNode++;
				Node node;
				node.idRegion = newPolygon.id;
				node.isRoot = false;
				node.idNode = idNode;
				for(int k = 0; k < tree.nodes.size(); k++){
					if(tree.nodes.at(k).idRegion == currentPolygon.id){
						node.idParents = tree.nodes.at(k).idNode;
						break;
					}
				}
				cout << "IDs " << node.idParents << " " << node.idNode << " " << node.idRegion << endl;
				cout << "----------------end---------------" << endl;
						
				tree.nodes.push_back(node);
				stackPolygon.push(newPolygon);
				regions.push_back(newPolygon);
			}
		}
	}
}

int getIndexByValue(vector<Point> points, Point value){
	for(int i = 0; i < points.size(); i++){
		if(points.at(i) == value){
			return i;
		}
	}
}

void getPolygon(vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, Region &connectedComponent)
{
	vector<Point> hull = connectedComponent.hull;
	bool onHullUp;

	if(hull.size() > 2)
	{
		if(connectedComponent.indexBreakPointHull > 1)
		{
			onHullUp = false;
			for(int i = 0; i < connectedComponent.indexBreakPointHull - 1; i++)
			{
				cout << "get polygon " << i << endl;
				if(isConcavePart(myPoints, numberPixelEnlarge, hull.at(i), hull.at(i + 1), onHullUp))
				{
					getPolygonInConcavePart(myPoints, connectedComponent, hull.at(i), 
						hull.at(i + 1), numberPixelEnlarge);
				}
				else
				{
					connectedComponent.polygon.push_back(hull.at(i));
				}
			}	
		}
		else
		{
			connectedComponent.polygon.push_back(hull.at(0));
		}

		if((hull.size() - connectedComponent.indexBreakPointHull) > 0)
		{
			onHullUp = true;
			for(int i = connectedComponent.indexBreakPointHull - 1; i < hull.size() - 1; i++)
			{
				cout << "get polygon " << i << endl;
				if(isConcavePart(myPoints, numberPixelEnlarge, hull.at(i), hull.at(i + 1), onHullUp))
				{
					getPolygonInConcavePart(myPoints, connectedComponent, hull.at(i), 
						hull.at(i + 1), numberPixelEnlarge);
				}
				else
				{
					connectedComponent.polygon.push_back(hull.at(i));
				}
			}	
		}
		else
		{
			connectedComponent.polygon.push_back(hull.at(connectedComponent.indexBreakPointHull + 1));
		}

		onHullUp = true;
		if(isConcavePart(myPoints, numberPixelEnlarge, hull.at(hull.size() - 1), hull.at(0), onHullUp))
		{
			getPolygonInConcavePart(myPoints, connectedComponent, hull.at(hull.size() - 1), 
				hull.at(0), numberPixelEnlarge);
		}
		else
		{
			connectedComponent.polygon.push_back(hull.at(hull.size() - 1));
		}
	}
	else
	{
		connectedComponent.polygon = hull;
	}
	cout << "finish polygon" << endl;
}

void sortPoint(Point &startPoint, Point &endPoint)
{
	if(startPoint[0] > endPoint[0])
	{
		Point tmp = startPoint;
		startPoint = endPoint;
		endPoint = tmp;
	}
}

bool testLinePolygon(vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, 
	Point startPoint, Point endPoint, vector<Point> &boundery8, int startIndexBoundery, int endIndexBoundery)
{
	int labelCurrent = myPoints.at(startPoint[0] + numberPixelEnlarge).at(startPoint[1] + numberPixelEnlarge).label; 

	if(startPoint[0] == endPoint[0])
	{
		int x = startPoint[0];
		int yStart, yEnd;
		yStart = getMinInteger(startPoint[1], endPoint[1]);
		yEnd = getMaxInteger(startPoint[1], endPoint[1]);

		for(int y = yStart; y <= yEnd; y++)
		{
			if(myPoints[x + numberPixelEnlarge][y + numberPixelEnlarge].label != labelCurrent)
			{
				return false;
			}
		}

		for(int i = startIndexBoundery + 1; i < endIndexBoundery; i++)
		{
			Point p = boundery8.at(i);
			int x = p[0], y = p[1];

			if(startPoint[1] < endPoint[1])
			{
				if(x > startPoint[0])
				{
					return false;
				}
			}
			else
			{
				if(x < startPoint[0])
				{
					return false;
				}
			}
		}

		return true;
	}
	else
	{
		int xStart = getMinInteger(startPoint[0], endPoint[0]);
		int xEnd = getMaxInteger(startPoint[0], endPoint[0]);
		double slope = (double)(startPoint[1] - endPoint[1]) / (startPoint[0] - endPoint[0]);

		
		if(abs(slope) <= 1)
		{
			for(int x = xStart; x <= xEnd; x++)
			{
				int y;

				if(startPoint[0] < endPoint[0])
				{
					y = ceil(slope * x - slope * startPoint[0] + startPoint[1] - DELTA);
				}
				else
				{
					y = floor(slope * x - slope * startPoint[0] + startPoint[1] + DELTA);
				}

				if(myPoints[x + numberPixelEnlarge][y + numberPixelEnlarge].label != labelCurrent)
				{
					return false;
				}
			}
		}
		else
		{
			int yStart = getMinInteger(startPoint[1], endPoint[1]);
			int yEnd = getMaxInteger(startPoint[1], endPoint[1]);
			double invSlope = (double)(startPoint[0] - endPoint[0]) / (startPoint[1] - endPoint[1]);

			for(int y = yStart; y <= yEnd; y++)
			{
				int x;

				if(startPoint[1] < endPoint[1])
				{
					x = floor(invSlope * y - invSlope * startPoint[1] + startPoint[0] + DELTA);
				}
				else 
				{
					x = ceil(invSlope * y - invSlope * startPoint[1] + startPoint[0] - DELTA);
				}

				if(myPoints[x + numberPixelEnlarge][y + numberPixelEnlarge].label != labelCurrent)
				{
					return false;
				}
			}
		}

		for(int i = startIndexBoundery + 1; i < endIndexBoundery; i++)
		{
			Point p = boundery8.at(i);
			int x = p[0], y = p[1];

			if(startPoint[0] < endPoint[0])
			{
				if((slope * x - slope * startPoint[0] + startPoint[1] - y) > 0)
				{
					return false;
				}
			}
			else
			{
				if((slope * x - slope * startPoint[0] + startPoint[1] - y) < 0)
				{
					return false;
				}
			}
		}

		return true;
	}
}

void getPolygonInConcavePart(vector<vector<MyPoint>> &myPoints, Region &connectedComponent, 
	Point startPoint, Point endPoint, int numberPixelEnlarge)
{
	vector<Point> boundery8 = connectedComponent.boundery8;

	int indexBoundery;
	for(indexBoundery = 0; indexBoundery < boundery8.size(); indexBoundery++)
	{
		if(boundery8.at(indexBoundery) == startPoint)
		{
			break;
		}
	}
	cout << "getPolygonInConcavePart " << indexBoundery << " " << startPoint << " " << endPoint << endl;

	vector<Point> polygon;
	polygon.push_back(startPoint);
	Point nextPoint;
	int startIndexBoundery = indexBoundery;
	int index;

	for(index = indexBoundery + 1; ; index++)
	{
		if(index >= boundery8.size())
		{
			index = 0;
		}

		if(startIndexBoundery >= boundery8.size() - 1 || startIndexBoundery < 0)
		{
			startIndexBoundery = 0;
		}

		if(testLinePolygon(myPoints, numberPixelEnlarge, polygon.at(polygon.size() - 1), boundery8.at(index),
			boundery8, startIndexBoundery, index))
		{
			nextPoint = boundery8.at(index);
		}
		else
		{
			startIndexBoundery = index - 1;
			index--;
			polygon.push_back(nextPoint);
		}
		//cout << "currentPoint " << boundery8.at(index) << endl;
		if(boundery8.at(index) == endPoint)
		{
			break;
		}
	}

	for(int i = 0; i < polygon.size(); i++)
	{
		connectedComponent.polygon.push_back(polygon.at(i));
	}
}

bool isConcavePart(vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, 
	Point startPoint, Point endPoint, bool onHullUp)
{

	int labelCurrent = myPoints.at(startPoint[0] + numberPixelEnlarge).at(startPoint[1] + numberPixelEnlarge).label; 

	if(startPoint[0] == endPoint[0])
	{
		int x = startPoint[0];
		int yStart, yEnd;
		yStart = getMinInteger(startPoint[1], endPoint[1]);
		yEnd = getMaxInteger(startPoint[1], endPoint[1]);

		for(int y = yStart; y <= yEnd; y++)
		{
			if(myPoints[x + numberPixelEnlarge][y + numberPixelEnlarge].label != labelCurrent)
			{
				return true;
			}
		}

		return false;
	}
	else
	{
		int xStart = getMinInteger(startPoint[0], endPoint[0]);
		int xEnd = getMaxInteger(startPoint[0], endPoint[0]);
		double slope = (double)(startPoint[1] - endPoint[1]) / (startPoint[0] - endPoint[0]);

		if(abs(slope) <= 1)
		{
			for(int x = xStart; x <= xEnd; x++)
			{
				int y;

				if(onHullUp)
				{
					y = floor(slope * x - slope * startPoint[0] + startPoint[1] + DELTA);
				}
				else
				{
					y = ceil(slope * x - slope * startPoint[0] + startPoint[1] - DELTA);
				}

				if(myPoints[x + numberPixelEnlarge][y + numberPixelEnlarge].label != labelCurrent)
				{
					return true;
				}
			}

			return false;
		}
		else
		{
			int yStart = getMinInteger(startPoint[1], endPoint[1]);
			int yEnd = getMaxInteger(startPoint[1], endPoint[1]);
			double invSlope = (double)(startPoint[0] - endPoint[0]) / (startPoint[1] - endPoint[1]);

			for(int y = yStart; y <= yEnd; y++)
			{
				int x;

				if((onHullUp == true && invSlope < 0) || (onHullUp == false && invSlope > 0))
				{
					x = floor(invSlope * y - invSlope * startPoint[1] + startPoint[0] + DELTA);
				}
				else if ((onHullUp == true && invSlope > 0) || (onHullUp == false && invSlope < 0))
				{
					x = ceil(invSlope * y - invSlope * startPoint[1] + startPoint[0] - DELTA);
				}

				if(myPoints[x + numberPixelEnlarge][y + numberPixelEnlarge].label != labelCurrent)
				{
					return true;
				}
			}

			return false;
		}
	}
}

void getBoundery8(vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, Region &connectedComponent)
{
	vector<Point> hull = connectedComponent.hull;
	vector<Point> boundery;

	if(hull.size() > 1)
	{
		Point topLeft = getPointTopLeft(hull);

		Point currentPoint = topLeft;
			
		int dir = 7;
		currentPoint = getNextBounderyPoint(topLeft, dir, currentPoint, myPoints, numberPixelEnlarge);
		boundery.push_back(currentPoint);

		while(currentPoint != topLeft)
		{
			currentPoint = getNextBounderyPoint(topLeft, dir, currentPoint, myPoints, numberPixelEnlarge);
			boundery.push_back(currentPoint);
		}

		connectedComponent.boundery8 = boundery;
	}
	else
	{
		connectedComponent.boundery8 = hull;
	}
}

Point getNextBounderyPoint(Point topLeft, int &dir, Point currentPoint, vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge)
{
	if(dir % 2 == 0)
	{
		dir = (dir + 7) % 8;
	}
	else
	{
		dir = (dir + 6) % 8;
	}

	vector<Point> neighbors;
	neighbors.push_back(Point(currentPoint[0] + 1, currentPoint[1]    ));
	neighbors.push_back(Point(currentPoint[0] + 1, currentPoint[1] + 1));
	neighbors.push_back(Point(currentPoint[0]    , currentPoint[1] + 1));
	neighbors.push_back(Point(currentPoint[0] - 1, currentPoint[1] + 1));
	neighbors.push_back(Point(currentPoint[0] - 1, currentPoint[1]    ));
	neighbors.push_back(Point(currentPoint[0] - 1, currentPoint[1] - 1));
	neighbors.push_back(Point(currentPoint[0]    , currentPoint[1] - 1));
	neighbors.push_back(Point(currentPoint[0] + 1, currentPoint[1] - 1));

	while(myPoints[neighbors.at(dir)[0] + numberPixelEnlarge][neighbors.at(dir)[1] + numberPixelEnlarge].label
			!= myPoints[topLeft[0] + numberPixelEnlarge][topLeft[1] + numberPixelEnlarge].label)
	{
		if(dir < 7)
		{
			dir++;
		}else
		{
			dir = 0;
		}
	}

	return neighbors.at(dir);
}

Point getPointTopLeft(vector<Point> &points)
{
	for(int i = 0; i < points.size(); i++)
	{
		Point tmp;
		for(int j = i + 1; j < points.size(); j++)
		{
			if(points.at(i)[1] > points.at(j)[1])
			{
				tmp = points.at(i);
				points.at(i) = points.at(j);
				points.at(j) = tmp;	
			}
		}
	}

	Point pointTopLeft = points.at(points.size() - 1);

	for(int i = points.size() - 2; i >= 0; i--)
	{
		if(points.at(i)[1] != pointTopLeft[1])
		{
			break;
		}

		if(points.at(i)[0] < pointTopLeft[0])
		{
			pointTopLeft = points.at(i);
		}
	}

	return pointTopLeft;
}

void getTransformationDomain(Point p1, Point p2, Point p3, Point p4, Point &p5, Point &p6, double angle, int dx, int dy)
{
	vector<Point> coinPoints;

 	DoublePoint dp1;
 	dp1.x = p1[0];
 	dp1.y = p1[1];

 	DoublePoint _p1 = rigidMotion(dp1, angle, dx, dy);
 	coinPoints.push_back(Point(_p1.x, _p1.y));

 	DoublePoint dp2;
 	dp2.x = p2[0];
 	dp2.y = p2[1];

 	DoublePoint _p2 = rigidMotion(dp2, angle, dx, dy);
 	coinPoints.push_back(Point(_p2.x, _p2.y));

 	DoublePoint dp3;
 	dp3.x = p3[0];
 	dp3.y = p3[1];

 	DoublePoint _p3 = rigidMotion(dp3, angle, dx, dy);
 	coinPoints.push_back(Point(_p3.x, _p3.y));

 	DoublePoint dp4;
 	dp4.x = p4[0];
 	dp4.y = p4[1];

 	DoublePoint _p4 = rigidMotion(dp4, angle, dx, dy);
 	coinPoints.push_back(Point(_p4.x, _p4.y));

 	p5 = coinPoints.at(0); 
 	p6 = coinPoints.at(0);

 	for(int i = 1; i < coinPoints.size(); i++)
 	{
 		p5[0] = getMinInteger(p5[0], coinPoints.at(i)[0]);
	 	p5[1] = getMinInteger(p5[1], coinPoints.at(i)[1]);

	 	p6[0] = getMaxInteger(p6[0], coinPoints.at(i)[0]);
	 	p6[1] = getMaxInteger(p6[1], coinPoints.at(i)[1]);
 	}
}

void calculateNbPixelInHullOfOriginalImage(Tree tree, vector<Region> &regions)
{
	for(int i = 0; i < tree.nodes.size(); i++)
	{
		Node currentNode = tree.nodes.at(i);
		vector<Node> allSubNodes = currentNode.subnodes;
		queue<Node> queueSubNodes;
		for(int j = 0; j < allSubNodes.size(); j++)
		{
			queueSubNodes.push(allSubNodes.at(j));
		}
		while(!queueSubNodes.empty())
		{
			Node node = queueSubNodes.front();
			queueSubNodes.pop();
			for(int k = 0; k < node.subnodes.size(); k++)
			{
				queueSubNodes.push(node.subnodes.at(k));
				allSubNodes.push_back(node.subnodes.at(k));
			}
		}

		regions.at(currentNode.idRegion).nbPixelInHull = regions.at(currentNode.idRegion).points.size();

		for(int j = 0; j < allSubNodes.size(); j++)
		{
			regions.at(currentNode.idRegion).nbPixelInHull += regions.at(allSubNodes.at(j).idRegion).points.size();
		}
	}
}

void calculateComplementaryInformation(vector<Point> &hull, double &perimeter, double &area)
{ 
	if (hull.size() == 1)
	{
		perimeter = 0;
		area = 0;
	} else if(hull.size() == 2)
	{
		perimeter += calculateDistance(hull.at(0), hull.at(1)); 
		area = 0;
	} else
	{
		Point p1 = hull.at(0);

		perimeter += calculateDistance(hull.at(0), hull.at(1)); 

		for(int j = 1; j < hull.size() - 1; j++)
		{
			Point p2 = hull.at(j);
			Point p3 = hull.at(j + 1);

			perimeter += calculateDistance(p2, p3); 
			area += calculateAreaTriangle(p1, p2, p3);
		}

		perimeter += calculateDistance(hull.at(0), hull.at(hull.size() - 1)); 
	}
}

double calculateDistance(Point p1, Point p2)
{
	int dx = p1[0] - p2[0];
	int dy = p1[1] - p2[1];

	return sqrt(dx * dx + dy * dy);
}

double calculateAreaTriangle(Point p1, Point p2, Point p3)
{
	double a = calculateDistance(p1, p2);
	double b = calculateDistance(p2, p3);
	double c = calculateDistance(p3, p1);

	return (sqrt((a + b + c) * (a + b - c) * (b + c - a) * (c + a - b)) / 4);
}

void getBoundery(vector<Point> &hull, vector<vector<MyPoint>> &myPoints, int &nbPointBoundary8, int &nbPointBoundary4, 
	int labelCurrent, int labelParent, int deltaX, int deltaY)
{
	if (hull.size() == 1)
	{
		nbPointBoundary8 = 1;
		nbPointBoundary4 = 1;
	} else if(hull.size() == 2)
	{
		DoublePoint p1, p2;

		p1.x = hull.at(0)[0];
		p1.y = hull.at(0)[1];

		p2.x = hull.at(1)[0];
		p2.y = hull.at(1)[1];

		vector<Point> boundery8;

		tracePointInLine(boundery8, p1, p2, myPoints, deltaX, deltaY, labelCurrent);

		nbPointBoundary8 = boundery8.size();

		bool ok = true;

		for(int i = 0; i < boundery8.size() - 1; i++)
		{
			if(isNotWellComposedInBoundery(boundery8.at(i), boundery8.at(i + 1)))
			{
				ok = false;
				break;
			}
		}

		if(ok)
		{
			nbPointBoundary4 = nbPointBoundary8;
		}
	} else
	{
		int countNotWellComposed = 0;
		traceBoundery(hull.at(0), myPoints, nbPointBoundary8, countNotWellComposed, labelCurrent, labelParent, deltaX, deltaY);
		nbPointBoundary4 = nbPointBoundary8 + countNotWellComposed;
	}

	cout << nbPointBoundary8 << " " << nbPointBoundary4 << endl;
}

void traceBoundery(Point startPoint, vector<vector<MyPoint>> &myPoints, int &nbPointBoundary8, int &countNotWellComposed, 
	int labelCurrent, int labelParent, int deltaX, int deltaY)
{
	stack<Point> stackBounderyPoint;
	stackBounderyPoint.push(startPoint);
	myPoints[startPoint[0] - deltaX][startPoint[1] - deltaY].countRepeat++;
	if(myPoints[startPoint[0] - deltaX][startPoint[1] - deltaY].countRepeat == 1)
	{
		nbPointBoundary8++;
	}

	while(!stackBounderyPoint.empty())
	{
		Point p = stackBounderyPoint.top();
		stackBounderyPoint.pop();

		//Get neighbor of boundery point
		vector<Point> neighbors;
		neighbors.push_back(Point(p[0] - 1, p[1] - 1));
		neighbors.push_back(Point(p[0]    , p[1] - 1));
		neighbors.push_back(Point(p[0] + 1, p[1] - 1));

		neighbors.push_back(Point(p[0] - 1, p[1]    ));
		neighbors.push_back(Point(p[0] + 1, p[1]    ));

		neighbors.push_back(Point(p[0] - 1, p[1] + 1));
		neighbors.push_back(Point(p[0]    , p[1] + 1));
		neighbors.push_back(Point(p[0] + 1, p[1] + 1));

		//Get next boundery points
		for(int i = 0; i < neighbors.size(); i++)
		{
			Point p0 = neighbors.at(i);

			if(myPoints[p0[0] - deltaX][p0[1] - deltaY].label == labelCurrent)
			{
				//Get neighbor4 of every neighbor of boundery point 
				vector<Point> neighbors4;

				neighbors4.push_back(Point(p0[0]    , p0[1] - 1));
				neighbors4.push_back(Point(p0[0] - 1, p0[1]    ));
				neighbors4.push_back(Point(p0[0] + 1, p0[1]    ));
				neighbors4.push_back(Point(p0[0]    , p0[1] + 1));

				bool ok = false;
				//If neighbor4 isn't current component and p0 never in stack, take p0 as a boundery point 
				for(int j = 0; j < neighbors4.size(); j++)
				{
					if((myPoints[neighbors4.at(j)[0] - deltaX][neighbors4.at(j)[1] - deltaY].label == labelParent) &&
						(myPoints[p0[0] - deltaX][p0[1] - deltaY].countRepeat == 0))
					{
						nbPointBoundary8++;
						stackBounderyPoint.push(p0);
						myPoints[p0[0] - deltaX][p0[1] - deltaY].countRepeat++;
						ok = true;
						break;
					}
				}

				if(!ok)
				{
					//Get neighbor8 of every neighbor of boundery point 
					vector<Point> neighbors8;

					neighbors8.push_back(Point(p0[0] - 1, p0[1] + 1));
					neighbors8.push_back(Point(p0[0] + 1, p0[1] + 1));
					neighbors8.push_back(Point(p0[0] - 1, p0[1] - 1));
					neighbors8.push_back(Point(p0[0] + 1, p0[1] - 1));

					for(int j = 0; j < neighbors8.size(); j++)
					{
						if((myPoints[neighbors8.at(j)[0] - deltaX][neighbors8.at(j)[1] - deltaY].label == labelParent) &&
							(myPoints[p0[0] - deltaX][p0[1] - deltaY].countRepeat == 0))
						{
							countNotWellComposed++;
							myPoints[p0[0] - deltaX][p0[1] - deltaY].countRepeat++;
							break;
						}
					}
				}
			}
		}
	}
}

bool isNotWellComposedInBoundery(Point p1, Point p2)
{
	if((p1[0] != p2[0]) && (p1[1] != p2[1]))
	{
		return false;
	}

	return true;
}

vector<Region> transformationPointByHull(vector<Region> &regions, vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY,
 	double angle, int dx, int dy)
{
	vector<Region> regionsTransformationByHull;
	for (int i = 0; i < regions.size(); ++i)
	{
		vector<Point> hull = regions.at(i).hull;

		Region r;
		r = regions.at(i);
		r.points.clear();
		r.hull.clear();
		r.perimeter = 0;
		r.area = 0;
		r.nbPointBoundary8 = 0;
		r.nbPointBoundary4 = 0;

		vector<DoublePoint> hullTransformation;
		for(int j = 0; j < hull.size(); j++)
		{
			DoublePoint p;
 			p.x = hull.at(j)[0];
 			p.y = hull.at(j)[1];

			p = rigidMotion(p, angle, dx, dy);
			hullTransformation.push_back(p);
		}

		if (hullTransformation.size() == 1)
		{
			Point p(hullTransformation.at(0).x, hullTransformation.at(0).y);
			r.points.push_back(p);
			myPointsTransformationByHull[p[0] - deltaX][p[1] - deltaY].label = r.id;
		} else if(hullTransformation.size() == 2)
		{
			tracePointInLine(r.points, hullTransformation.at(0), hullTransformation.at(1), myPointsTransformationByHull, deltaX, deltaY, r.id);
		} else
		{
			getPointInDoubleHull(r.points, hullTransformation, myPointsTransformationByHull, deltaX, deltaY, r.id);
		}

		getConvexHull(r);
		calculateComplementaryInformation(r.hull, r.perimeter, r.area);
		r.nbPixelInHull = r.points.size();

		regionsTransformationByHull.push_back(r);
	}

	return regionsTransformationByHull;
}

void tracePointInLine(vector<Point> &points, DoublePoint p1, DoublePoint p2, 
	vector<vector<MyPoint>> &myPoints, int deltaX, int deltaY, int label)
{
	if (round(p1.y) == round(p2.y))
	{
		int xStart = getMinInteger(ceil(p1.x - DELTA), ceil(p2.x - DELTA));
		double xEnd = p1.x;
		if(p1.x < p2.x)
		{
			xEnd = p2.x;
		}
		
		for (int i = xStart; i <= xEnd; ++i)
	    {
	    	Point p(i, round(p1.y));
	    	points.push_back(p);
	    	myPoints[p[0] - deltaX][p[1] - deltaY].label = label;
	    }
	    return;
	}

	int rowStart = floor(p1.y + DELTA);
	double rowEnd = p2.y + DELTA;
	if(p1.y > p2.y)
	{
		rowStart = ceil(p2.y - DELTA);
		rowEnd = p1.y + DELTA;
	}

    double invSlope = (double)(p2.x - p1.x) / (p2.y - p1.y);

    for (int i = rowStart; i <= rowEnd; ++i)
    {
    	double x = i * invSlope - p2.y * invSlope + p2.x;
    	Point p(round(x), i);
    	points.push_back(p);
    	myPoints[p[0] - deltaX][p[1] - deltaY].label = label;
    }
}

void getPointInDoubleHull(vector<Point> &points, vector<DoublePoint> &hull, 
	vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY, int label)
{
	for (int j = 1; j < hull.size() - 1; ++j)
	{
		getPointInFullTriangle(points, hull[0], hull[j], hull[j+1], myPointsTransformationByHull, deltaX, deltaY, label);
	}
}

void getPointInLine(vector<Point> &points, double leftPoint_X, double leftPoint_Y, double rightPoint_X, double rightPoint_Y, 
	vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY, int label)
{
	double x_start, x_end;
	int row;

	x_start = leftPoint_X;
	x_end = rightPoint_X;

	row = leftPoint_Y;

	for( int i = ceil(x_start - DELTA); i <= x_end + DELTA; i++)
	{
	    Point p(i, row);
	    points.push_back(p);
	    myPointsTransformationByHull[p[0] - deltaX][p[1] - deltaY].label = label;
	}
}


void getPointInFlatTopTriangle(vector<Point> &points, DoublePoint p1, DoublePoint p2, DoublePoint p3, 
	vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY, int label)
{
    DoublePoint tmp;

    if(p2.x > p3.x)
    {
    	tmp = p2;
    	p2 = p3;
    	p3 = tmp;
    }

    double invSlope1 = (double)(p2.x - p1.x) / (p2.y - p1.y);
    double invSlope2 = (double)(p3.x - p1.x) / (p3.y - p1.y);

    double leftPoint_X, leftPoint_Y, rightPoint_X, rightPoint_Y;

    for (int currentLine = ceil(p1.y - DELTA); currentLine <= p2.y + DELTA; currentLine++)
    {
     	leftPoint_Y = currentLine;
     	leftPoint_X = leftPoint_Y * invSlope1 - p1.y * invSlope1 + p1.x;

     	rightPoint_Y = currentLine;
     	rightPoint_X = rightPoint_Y * invSlope2 - p1.y * invSlope2 + p1.x;

     	getPointInLine(points, leftPoint_X, leftPoint_Y, rightPoint_X, rightPoint_Y, myPointsTransformationByHull, deltaX, deltaY, label);
    }
}


void getPointInFlatBottomTriangle(vector<Point> &points, DoublePoint p1, DoublePoint p2, DoublePoint p3, 
	vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY, int label)
{
    DoublePoint tmp;

    if(p1.x > p2.x)
    {
    	tmp = p1;
    	p1 = p2;
    	p2 = tmp;
    }
    double invSlope1 = (double)(p3.x - p1.x) / (p3.y - p1.y);
    double invSlope2 = (double)(p3.x - p2.x) / (p3.y - p2.y);

    double leftPoint_X, leftPoint_Y, rightPoint_X, rightPoint_Y;

    for (int currentLine = floor(p3.y + DELTA); currentLine >= p1.y - DELTA; currentLine--)
    {
    	leftPoint_Y = currentLine;
    	leftPoint_X = leftPoint_Y * invSlope1 - p3.y * invSlope1 + p3.x;

    	rightPoint_Y = currentLine;
     	rightPoint_X = rightPoint_Y * invSlope2 - p3.y * invSlope2 + p3.x;

    	getPointInLine(points, leftPoint_X, leftPoint_Y, rightPoint_X, rightPoint_Y, myPointsTransformationByHull, deltaX, deltaY, label);
    }
}

void getPointInFullTriangle(vector<Point> &points, DoublePoint p1c, DoublePoint p2c, DoublePoint p3c, 
	vector<vector<MyPoint>> &myPointsTransformationByHull, int deltaX, int deltaY, int label)
{
    // Sorts the points p1, p2 and p3 and stores them in order in p1c, p2c and p3c
    sortTwoDoublePoint(p1c, p2c);
   	sortTwoDoublePoint(p2c, p3c);
 	sortTwoDoublePoint(p1c, p2c);
   	sortTwoDoublePoint(p2c, p3c);

    // Checks if it's a triangle with flat top
    if ((int)(p2c.y) == (int)(p3c.y))
    {
      getPointInFlatTopTriangle(points, p1c, p2c, p3c, myPointsTransformationByHull, deltaX, deltaY, label);
    }

    // Checks if it's a triangle with flat bottom
    else if ((int)(p1c.y) == (int)(p2c.y))
    {
      getPointInFlatBottomTriangle(points, p1c, p2c, p3c, myPointsTransformationByHull, deltaX, deltaY, label);
    }

    // General case - cut the triangle into two parts: flat bottom and flat top
    else
    {
      DoublePoint pm;
      pm.y = p2c.y;
      double invSlope1 = (double)(p3c.x - p1c.x) / (p3c.y - p1c.y);
      pm.x = pm.y * invSlope1 - p3c.y * invSlope1 + p3c.x;

      getPointInFlatTopTriangle(points, p1c, p2c, pm, myPointsTransformationByHull, deltaX, deltaY, label);

      getPointInFlatBottomTriangle(points, p2c, pm, p3c, myPointsTransformationByHull, deltaX, deltaY, label);
    }
}


vector<Region> transformationPointByPoint(vector<Region> &regions, double angle, int dx, int dy, 
	vector<vector<MyPoint>> &myPointsTransformationPointByPoint, int deltaX, int deltaY)
{
	vector<Region> transformationRegions;
	for (int i = 0; i < regions.size(); i++)
	{
		Region r = regions.at(i);
		r.points.clear();
		r.hull.clear();
		r.perimeter = 0;
		r.area = 0;
		r.nbPointBoundary8 = 0;
		r.nbPointBoundary4 = 0;

		vector<Point> points = regions.at(i).points;
		vector<Point> hull = regions.at(i).hull;

		for(int j = 0; j < points.size(); j++)
		{
			getPointsBackward(r.nbPixelInHull, r.points, points.at(j), r.id, angle, dx, dy, myPointsTransformationPointByPoint, deltaX, deltaY);
		}

		initCountRepeat(myPointsTransformationPointByPoint);

		getConvexHull(r);
		calculateComplementaryInformation(r.hull, r.perimeter, r.area);

		transformationRegions.push_back(r);
	}
	return transformationRegions;
}

void initCountRepeat(vector<vector<MyPoint>> &myPointsTransformationPointByPoint)
{
	for(int i = 0; i < myPointsTransformationPointByPoint.size(); i++)
	{
		for(int j = 0; j < myPointsTransformationPointByPoint.at(0).size(); j++)
		{
			myPointsTransformationPointByPoint[i][j].countRepeat = 0;
		}
	}
}

void getPointsBackward(int &nbPixelInHull, vector<Point> &points, Point point, int label, double angle, int dx, int dy,
	vector<vector<MyPoint>> &myPointsTransformationPointByPoint, int deltaX, int deltaY)
{
	double minX, maxX, minY, maxY;

	DoublePoint p1;
	p1.x = point[0] - 0.5;
	p1.y = point[1] - 0.5;
	p1 = rigidMotion(p1, angle, dx, dy);

	minX = p1.x;
	maxX = p1.x;
	minY = p1.y;
	maxY = p1.y;

	DoublePoint p2;
	p2.x = point[0] - 0.5;
	p2.y = point[1] + 0.5;
	p2 = rigidMotion(p2, angle, dx, dy);

	minX = getMinDouble(minX, p2.x);
	maxX = getMaxDouble(maxX, p2.x);
	minY = getMinDouble(minY, p2.y);
	maxY = getMaxDouble(maxY, p2.y);

	DoublePoint p3;
	p3.x = point[0] + 0.5;
	p3.y = point[1] - 0.5;
	p3 = rigidMotion(p3, angle, dx, dy);

	minX = getMinDouble(minX, p3.x);
	maxX = getMaxDouble(maxX, p3.x);
	minY = getMinDouble(minY, p3.y);
	maxY = getMaxDouble(maxY, p3.y);

	DoublePoint p4;
	p4.x = point[0] + 0.5;
	p4.y = point[1] + 0.5;
	p4 = rigidMotion(p4, angle, dx, dy);

	minX = getMinDouble(minX, p4.x);
	maxX = getMaxDouble(maxX, p4.x);
	minY = getMinDouble(minY, p4.y);
	maxY = getMaxDouble(maxY, p4.y);

	int beginX = ceil(minX - 0.001);
	int endX = floor(maxX + 0.001);
	int beginY = ceil(minY - 0.001);
	int endY = floor(maxY + 0.001);

	for(int i = beginX; i <= endX; i++)
	{
		for(int  j = beginY; j <= endY; j++)
		{
			points.push_back(Point(i, j));
			myPointsTransformationPointByPoint[i - deltaX][j - deltaY].countRepeat++;
			myPointsTransformationPointByPoint[i - deltaX][j - deltaY].label = label;
			if (myPointsTransformationPointByPoint[i - deltaX][j - deltaY].countRepeat == 1)
			{
				nbPixelInHull++;
			}
		}
	}
}

void rotation(DoublePoint input, DoublePoint &output, double angle)
{
	output.x = input.x * cos(angle) - input.y * sin(angle);
	output.y = input.x * sin(angle) + input.y * cos(angle);
}

void transformation(DoublePoint input, DoublePoint &output, int dx, int dy)
{
	output.x = input.x + dx;
	output.y = input.y + dy;
}

DoublePoint rigidMotion(DoublePoint input, double angle, int dx, int dy)
{
	DoublePoint rotationPoint, transformationPoint;
	rotation(input, rotationPoint, angle);
	transformation(rotationPoint, transformationPoint, dx, dy);
	return transformationPoint;
}

void intersectionVectorV1ToVectorV2(vector<int> &v1, vector<int> & v2)
{
	for(int k1 = 0; k1 < v1.size(); k1++)
	{
	    bool ok = false;
	    for(int k2 = 0; k2 < v2.size(); k2++)
	    {
		    if (v1[k1] == v2[k2])
		    {
		        ok = true;
		        break;
		    }
	    }

	    if (!ok)
	    {
	    	removeElementByValueFromVector(v1, v1[k1]);
	    }
	}
}


void removeElementByValueFromVector(vector<int> &v, int value)
{
	for(vector<int>::iterator iter = v.begin(); iter != v.end(); ++iter)
	{
	    if(*iter == value)
	    {
	        v.erase(iter);
	    }
	}
}

void intersectionTwoVectors(vector<int> &v1, vector<int> &v2)
{
	intersectionVectorV1ToVectorV2(v1, v2);
	intersectionVectorV1ToVectorV2(v2, v1);
}

void initValueVector(vector<int> &v, int value, int size)
{
	for(int i = 0; i < size; i++)
	{
	    v.push_back(value);
	}
}

void getAdjacencyTree(Domain domain, vector<Region> &regions, Tree &tree, int numberPixelEnlarge, 
	vector<vector<MyPoint>> &myPoints)
{
    for (int i = 0; i < regions.size(); ++i)
  	{
	    vector<int> verticalPossibleUpperParents;
		initValueVector(verticalPossibleUpperParents, regions.at(i).id, 1);

		vector<int> verticalPossibleLowerParents;
		initValueVector(verticalPossibleLowerParents, regions.at(i).id, 1);

		vector<int> horizontalPossibleLeftParents;
		initValueVector(horizontalPossibleLeftParents, regions.at(i).id, 1);

		vector<int> horizontalPossibleRightParents;
		initValueVector(horizontalPossibleRightParents, regions.at(i).id, 1);

		int xLeft  = regions.at(i).points.at(0)[0]; 
		int xRight = regions.at(i).points.at(0)[0]; 
		int yUpper = regions.at(i).points.at(0)[1]; 
		int yLower = regions.at(i).points.at(0)[1];

		while(yUpper < domain.upperBound()[1])
		{
			if (myPoints[regions.at(i).points.at(0)[0] + numberPixelEnlarge][yUpper + numberPixelEnlarge].label != 
			    myPoints[regions.at(i).points.at(0)[0] + numberPixelEnlarge][yUpper + numberPixelEnlarge + 1].label)
			{
			    verticalPossibleUpperParents.push_back(
			        myPoints[regions.at(i).points.at(0)[0] + numberPixelEnlarge][yUpper + numberPixelEnlarge + 1].label);
			}
		           
		    yUpper++;
		}

		while(yLower > domain.lowerBound()[1])
		{
		    if (myPoints[regions.at(i).points.at(0)[0] + numberPixelEnlarge][yLower + numberPixelEnlarge].label != 
		        myPoints[regions.at(i).points.at(0)[0] + numberPixelEnlarge][yLower + numberPixelEnlarge - 1].label)
		    {
		        verticalPossibleLowerParents.push_back(
		            myPoints[regions.at(i).points.at(0)[0] + numberPixelEnlarge][yLower + numberPixelEnlarge - 1].label);
		    }
		            
		    yLower--;
		}

		while(xRight < domain.upperBound()[0])
		{
		    if (myPoints[xRight + numberPixelEnlarge][regions.at(i).points.at(0)[1] + numberPixelEnlarge].label != 
		        myPoints[xRight + numberPixelEnlarge + 1][regions.at(i).points.at(0)[1] + numberPixelEnlarge].label)
		    {
		        horizontalPossibleRightParents.push_back(
		            myPoints[xRight + numberPixelEnlarge + 1][regions.at(i).points.at(0)[1] + numberPixelEnlarge].label);
		    }

		    xRight++;
		}

		while(xLeft > domain.lowerBound()[0])
		{
		    if (myPoints[xLeft + numberPixelEnlarge][regions.at(i).points.at(0)[1] + numberPixelEnlarge].label != 
		        myPoints[xLeft + numberPixelEnlarge - 1][regions.at(i).points.at(0)[1] + numberPixelEnlarge].label)
		    {
		        horizontalPossibleLeftParents.push_back(
		            myPoints[xLeft + numberPixelEnlarge - 1][regions.at(i).points.at(0)[1] + numberPixelEnlarge].label);
		    }

		    xLeft--;
		}

		intersectionTwoVectors(verticalPossibleLowerParents, verticalPossibleUpperParents);
		intersectionTwoVectors(horizontalPossibleLeftParents, horizontalPossibleRightParents);
		intersectionTwoVectors(horizontalPossibleLeftParents, verticalPossibleLowerParents);

		if (verticalPossibleLowerParents.size() > 1)
		{
			for(int k = verticalPossibleLowerParents.size() - 1; k >= 0; k--)
			{
			    if(verticalPossibleLowerParents.at(k) == verticalPossibleLowerParents.at(0))
			    {
				    tree.nodes.at(verticalPossibleLowerParents.at(k + 1)).subnodes.push_back(tree.nodes.at(verticalPossibleLowerParents.at(0)));
				    regions.at(tree.nodes.at(verticalPossibleLowerParents.at(0)).idRegion).labelParent = tree.nodes.at(verticalPossibleLowerParents.at(k + 1)).idRegion;
				    tree.nodes.at(verticalPossibleLowerParents.at(0)).isRoot = false;
				    break;
			    }
			}
		}
      		
  	}
}

void generateTextImage(vector<Region> &regions, Domain domain, int numberPixelEnlarge)
{
	ofstream outfile;
  	outfile.open("result/idImage.txt");
  	outfile << domain.upperBound()[0] << " " << domain.upperBound()[1] << endl;
  	outfile << domain.lowerBound()[0] << " " << domain.lowerBound()[1] << endl;

  	int width = domain.upperBound()[0] - domain.lowerBound()[0] + 1;
  	int height = domain.upperBound()[1] - domain.lowerBound()[1] + 1;

  	vector<vector<int>> idRegions( width, vector<int>(1, -1 ));
  	for(int i = 0; i < width; i++)
  	{
  		for(int j = 1; j < height; j++)
  		{
  			idRegions.at(i).push_back(-1);
  		}
  	}

  	for(int i = 0; i < regions.size(); i++)
  	{
  		vector<Point> points = regions.at(i).points;
		for (int j = 0; j < points.size(); ++j)
		{      
		    idRegions[points.at(j)[0] + numberPixelEnlarge][points.at(j)[1] + numberPixelEnlarge] = regions.at(i).id;        
		}
  	}

  	for(int i = 0; i < width; i++)
  	{
  		for(int j = 0; j < height - 1; j++)
  		{
  			outfile << idRegions[i][j] << setw(3);
  		}
  		outfile << idRegions[i][height - 1] << endl;
  	}

  	outfile.close();
}

void getConcaveTree(Board2D &aBoard, Domain domainImage, int idCurrentRegion, vector<Region> &regions, int &levelRegion, 
	vector<vector<MyPoint>> &myPoints, int numberPixelEnlarge, Tree &tree, int valuePixelComponent, ConstImageAdapterForThresholderImage &thresholderImage)
{
	//Test if a component is convex, in the case not-convex return an array complementery points and its domain
	vector<Point> complementaryPoints;
	int maxX = domainImage.lowerBound()[0];
	int maxY = domainImage.lowerBound()[1];
	int minX = domainImage.upperBound()[0];
	int minY = domainImage.upperBound()[1];

	if(checkHConvex(aBoard, regions.at(idCurrentRegion).id, regions.at(idCurrentRegion).hull, myPoints, complementaryPoints, maxX, maxY, minX, minY, numberPixelEnlarge))
	{
		regions.at(idCurrentRegion).isHConvex = true;
		return;
	}

	regions.at(idCurrentRegion).isHConvex = false;

	MyPoint p;
	vector<vector<MyPoint>> myNewPoints(domainImage.upperBound()[0] - domainImage.lowerBound()[0] + 1, vector<MyPoint>(1, p ));

	initMyPoint(domainImage, myNewPoints, -numberPixelEnlarge, -numberPixelEnlarge, INIT_LABEL);

	for (int i = 0; i < complementaryPoints.size(); ++i)
	{
		if ((int)thresholderImage(complementaryPoints.at(i)) 
			!= valuePixelComponent)
		{
			myNewPoints[complementaryPoints.at(i)[0] + numberPixelEnlarge][complementaryPoints.at(i)[1] + numberPixelEnlarge].isComponent = true;
		}
		else
		{
			int idRegion = myPoints[complementaryPoints.at(i)[0] + numberPixelEnlarge][complementaryPoints.at(i)[1] + numberPixelEnlarge].label;
			regions.at(idRegion).isSubComponent = true;
		}
	}

	Domain domain(Point(minX - numberPixelEnlarge, minY - numberPixelEnlarge), Point(maxX + numberPixelEnlarge, maxY + numberPixelEnlarge));
	 
	int beginRegion = regions.size();

	get4ConnectivityRegion(domain, myNewPoints, regions, levelRegion, numberPixelEnlarge);

	for (int i = beginRegion; i < regions.size(); ++i)
	{
		Point p = regions.at(i).hull.at(0);
		Point left(p[0] - 1, p[1]);
		Point right(p[0] + 1, p[1]);
		Point up(p[0], p[1] + 1);
		Point down(p[0], p[1] - 1);

		if (myPoints[left[0] + numberPixelEnlarge][left[1] + numberPixelEnlarge].label == idCurrentRegion
			|| myPoints[right[0] + numberPixelEnlarge][right[1] + numberPixelEnlarge].label == idCurrentRegion
			|| myPoints[up[0] + numberPixelEnlarge][up[1] + numberPixelEnlarge].label == idCurrentRegion
			|| myPoints[down[0] + numberPixelEnlarge][down[1] + numberPixelEnlarge].label == idCurrentRegion)
		{
			Node node;
			node.idRegion = regions.at(i).id;

			for(int j = 0; j < tree.nodes.size(); j++)
			{
				if(tree.nodes.at(j).idRegion == idCurrentRegion)
				{
					tree.nodes.at(j).subnodes.push_back(node);
					break;
				}
			}

			tree.nodes.push_back(node);
		}
	}

	vector<Region> regionsCopy = regions;
	for (int i = beginRegion; i < regions.size(); ++i)
	{
		int levelRegion0 = levelRegion; 
		if ((regions[i].level == levelRegion - 1) && (!regionsCopy[i].isSubComponent))
		{
			//regionsCopy[i].isSubComponent = true;
			getConcaveTree(aBoard, domainImage, regions[i].id, regionsCopy, levelRegion0, myNewPoints, numberPixelEnlarge, 
				tree, abs(1 - valuePixelComponent), thresholderImage);
		}
	}

	regions = regionsCopy;
}

void get4ConnectivityRegion(Domain domain, vector<vector<MyPoint>> &myPoints, vector<Region> &regions, int &levelRegion, int numberPixelEnlarge)
{
	int labelCurrent = 0;
	int countRegion = 0;

  	int width = domain.upperBound()[0] - domain.lowerBound()[0];
  	int height = domain.upperBound()[1] - domain.lowerBound()[1];

  	vector<vector<int>> equivalentRegions(width * height , vector<int>( 1, -2 ));

  	for (int i = domain.lowerBound()[0]; i <= domain.upperBound()[0]; ++i)
  	{
    	for (int j = domain.lowerBound()[1]; j <= domain.upperBound()[1]; ++j)
    	{
      		//if (myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].isComponent)
      		//{
        		bool haveNeighbor = false;

        		if( j + numberPixelEnlarge - 1 >= 0)
        		{
          			if (myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge - 1].valuePixel ==
          				myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].valuePixel)
			        {
			            myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].label = myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge - 1].label;
			            haveNeighbor = true;
			        }
        		}

		        if( i + numberPixelEnlarge - 1 >= 0)
		        {
		          	if (myPoints[i + numberPixelEnlarge - 1][j + numberPixelEnlarge].valuePixel ==
		          		myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].valuePixel)
		          	{
		            	if (haveNeighbor)
		            	{
		              		if (myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge - 1].label != myPoints[i + numberPixelEnlarge - 1][j + numberPixelEnlarge].label)
		              		{
				                if (myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge - 1].label < myPoints[i + numberPixelEnlarge - 1][j + numberPixelEnlarge].label)
				                {
				                	myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].label = myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge - 1].label;				                  
				                	mergeTwoEquivalentRegion(equivalentRegions, myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].label, 
				                  	myPoints[i + numberPixelEnlarge - 1][j + numberPixelEnlarge].label, countRegion);
				                }
				                else
				                {
				                  	myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].label = myPoints[i + numberPixelEnlarge - 1][j + numberPixelEnlarge].label;
				                  	mergeTwoEquivalentRegion(equivalentRegions, myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].label, 
				                  	myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge - 1].label, countRegion);
				                }
				            }
				            else
				            {
				                myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].label = myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge - 1].label;
				            }
			            }
			            else
			            {
				            myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].label = myPoints[i + numberPixelEnlarge - 1][j + numberPixelEnlarge].label;
				            haveNeighbor = true;
			            }
		          	}
		        }

		        if (!haveNeighbor)
		        {
		          myPoints[i + numberPixelEnlarge][j + numberPixelEnlarge].label = labelCurrent;
		          equivalentRegions.at(countRegion).at(0) = labelCurrent;

		          countRegion++;
		          labelCurrent++;
		        }
      		//}
    	}
  	}
  	mergeEquivalentRegion(equivalentRegions, countRegion, regions, myPoints, domain, levelRegion, countRegion, numberPixelEnlarge);
}

void mergeTwoVector(vector<int> &v1, vector<int> &v2)
{
	for (int i = 0; i < v2.size(); ++i)
	{
		v1.push_back(v2.at(i));
	}
	v2.clear();
}

void mergeTwoEquivalentRegion(vector<vector<int>> &equivalentRegions, int label1, int label2, int countRegion)
{
	int idEquivalentRegionCurrentPixel = findIdEquivalentRegion(equivalentRegions, label1, countRegion);
	int idEquivalentRegionNeighborPixel = findIdEquivalentRegion(equivalentRegions, label2, countRegion);
	if (idEquivalentRegionNeighborPixel == -1)
	{
		equivalentRegions.at(idEquivalentRegionCurrentPixel).push_back(label2);
	}
	else if (idEquivalentRegionNeighborPixel != idEquivalentRegionCurrentPixel)
	{
	    if (idEquivalentRegionCurrentPixel > idEquivalentRegionNeighborPixel)
	    {
	    	mergeTwoVector(equivalentRegions.at(idEquivalentRegionNeighborPixel), equivalentRegions.at(idEquivalentRegionCurrentPixel));
	    }
	    else if (idEquivalentRegionCurrentPixel < idEquivalentRegionNeighborPixel)
	    {
	    	mergeTwoVector(equivalentRegions.at(idEquivalentRegionCurrentPixel), equivalentRegions.at(idEquivalentRegionNeighborPixel));
	    }
	}
}

int findIdEquivalentRegion(vector<vector<int>> regions, int labelCurrent, int numberRegion)
{
	int rows = numberRegion;
	for (int i = 0; i <= rows; ++i)
	{
	    int cols = regions.at(i).size();
	    for (int j = 0; j < cols; ++j)
	    {
	    	if (regions.at(i).at(j) == labelCurrent)
	    	{
	        	return i;
	    	}
	    }                 
	}
	return -1;
}

bool checkHConvex(Board2D &aBoard, int idRegion, vector<Point> &hull, vector<vector<MyPoint>> &myPoints, 
	vector<Point> &complementaryPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge)
{	    
	bool ok = true;

	for (int j = 1; j < hull.size() - 1; ++j)
	{
	    if (!testFullTriangle(hull[0], hull[j], hull[j+1], idRegion, complementaryPoints, myPoints, maxX, maxY, minX, minY, numberPixelEnlarge))
	    {
	       	ok = false;
	    }
	}

	return ok;
}

void initMyPoint(Domain domain, vector<vector<MyPoint>> &myPoints, int deltaX, int deltaY, int initLabel)
{
	MyPoint p;
	for (int i = domain.lowerBound()[0]; i <= domain.upperBound()[0]; ++i)
	{ 
		Point point(i, domain.lowerBound()[1]);
		p.coordinate = point;
		p.isComponent = false;
		p.label = initLabel;
		p.valuePixel = initLabel;
		myPoints.at(i - deltaX).at(0) = p;

	    for (int j = domain.lowerBound()[1] + 1; j <= domain.upperBound()[1]; ++j)
	    {
		    Point point(i, j);
		    p.coordinate = point;
		    p.isComponent = false;
		    p.label = initLabel;
		    p.valuePixel = initLabel;
		    myPoints.at(i - deltaX).push_back(p);
	    }
	}
}

int getMinInteger(int a, int b)
{
	return a < b ? a : b;
}

int getMaxInteger(int a, int b)
{
	return a > b ? a : b;
}

double getMinDouble(double a, double b)
{
	return a < b ? a : b;
}

double getMaxDouble(double a, double b)
{
	return a > b ? a : b;
}

bool testLine(double leftPoint_X, double leftPoint_Y, double rightPoint_X, double rightPoint_Y, int idRegion, 
	vector<Point> &complementaryPoints, vector<vector<MyPoint>> &myPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge)
{
	double x_start, x_end;
	int row;

	x_start = leftPoint_X;
	x_end = rightPoint_X;

	row = leftPoint_Y;
	maxY = getMaxInteger(row, maxY);
	minY = getMinInteger(row, minY);

	bool ok = true;

	for( int i = ceil(x_start - 0.001); i <= x_end + 0.001; i++)
	{
	    Point p(i, row);

	    if (myPoints[i + numberPixelEnlarge][row + numberPixelEnlarge].label != idRegion)
	    {
		    ok = false;
		    complementaryPoints.push_back(p);
		    
		    maxX = getMaxInteger(i, maxX);
		    minX = getMinInteger(i, minX);
	    }
	}
	return ok;
}


bool testFlatTopTriangle(DoublePoint p1, DoublePoint p2, DoublePoint p3, int idRegion, vector<Point> &complementaryPoints, 
	vector<vector<MyPoint>> &myPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge)
{
    DoublePoint tmp;

    if(p2.x > p3.x)
    {
      tmp = p2;
      p2 = p3;
      p3 = tmp;
    }

    double invSlope1 = (double)(p2.x - p1.x) / (p2.y - p1.y);
    double invSlope2 = (double)(p3.x - p1.x) / (p3.y - p1.y);

    double leftPoint_X, leftPoint_Y, rightPoint_X, rightPoint_Y;

    bool ok = true;

    for (int currentLine = p1.y; currentLine <= p2.y; currentLine++)
    {
      leftPoint_Y = currentLine;
      leftPoint_X = leftPoint_Y * invSlope1 - p1.y * invSlope1 + p1.x;

      rightPoint_Y = currentLine;
      rightPoint_X = rightPoint_Y * invSlope2 - p1.y * invSlope2 + p1.x;

      if (!testLine(leftPoint_X, leftPoint_Y, rightPoint_X, rightPoint_Y, idRegion, complementaryPoints, myPoints, maxX, maxY, minX, minY, numberPixelEnlarge))
      {
        ok = false;
      }
    }
    return ok;
}


bool testFlatBottomTriangle(DoublePoint p1, DoublePoint p2, DoublePoint p3, int idRegion, vector<Point> &complementaryPoints, 
	vector<vector<MyPoint>> &myPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge)
{
    DoublePoint tmp;

    if(p1.x > p2.x)
    {
      tmp = p1;
      p1 = p2;
      p2 = tmp;
    }

    double invSlope1 = (double)(p3.x - p1.x) / (p3.y - p1.y);
    double invSlope2 = (double)(p3.x - p2.x) / (p3.y - p2.y);

    double leftPoint_X, leftPoint_Y, rightPoint_X, rightPoint_Y;

    bool ok = true;

    for (int currentLine = p3.y; currentLine >= p1.y; currentLine--)
    {
      leftPoint_Y = currentLine;
      leftPoint_X = leftPoint_Y * invSlope1 - p3.y * invSlope1 + p3.x;

      rightPoint_Y = currentLine;
      rightPoint_X = rightPoint_Y * invSlope2 - p3.y * invSlope2 + p3.x;

      if (!testLine(leftPoint_X, leftPoint_Y, rightPoint_X, rightPoint_Y, idRegion, complementaryPoints, myPoints, maxX, maxY, minX, minY, numberPixelEnlarge))
      {
        ok = false;
      }
    }
    return ok;
}

void sortTwoDoublePoint(DoublePoint &p1, DoublePoint &p2)
{
	DoublePoint tmp;

	if(p1.y > p2.y)
    {
        tmp = p1;
        p1 = p2;
        p2 = tmp;
    }
}

bool testFullTriangle(Point p1, Point p2, Point p3, int idRegion, vector<Point> &complementaryPoints, 
	vector<vector<MyPoint>> &myPoints, int &maxX, int &maxY, int &minX, int &minY, int numberPixelEnlarge)
{
    DoublePoint p1c, p2c, p3c, tmp;

    p1c.x = p1[0];
    p1c.y = p1[1];

    p2c.x = p2[0];
    p2c.y = p2[1];

    p3c.x = p3[0];
    p3c.y = p3[1];

    // Sorts the points p1, p2 and p3 and stores them in order in p1c, p2c and p3c
    sortTwoDoublePoint(p1c, p2c);
   	sortTwoDoublePoint(p2c, p3c);
 	sortTwoDoublePoint(p1c, p2c);
   	sortTwoDoublePoint(p2c, p3c);

    // Checks if it's a triangle with flat top
    if (p2c.y == p3c.y)
    {
      return testFlatTopTriangle(p1c, p2c, p3c, idRegion, complementaryPoints, myPoints, maxX, maxY, minX, minY, numberPixelEnlarge);
    }

    // Checks if it's a triangle with flat bottom
    else if (p1c.y == p2c.y)
    {
      return testFlatBottomTriangle(p1c, p2c, p3c, idRegion, complementaryPoints, myPoints, maxX, maxY, minX, minY, numberPixelEnlarge);
    }

    // General case - cut the triangle into two parts: flat bottom and flat top
    else
    {
      DoublePoint pm;
      pm.y = p2c.y;
      double invSlope1 = (double)(p3c.x - p1c.x) / (p3c.y - p1c.y);
      pm.x = pm.y * invSlope1 - p3c.y * invSlope1 + p3c.x;

      bool ok = true;
 
      if(testFlatTopTriangle(p1c, p2c, pm, idRegion, complementaryPoints, myPoints, maxX, maxY, minX, minY, numberPixelEnlarge))
	  {
	    ok = testFlatBottomTriangle(p2c, pm, p3c, idRegion, complementaryPoints, myPoints, maxX, maxY, minX, minY, numberPixelEnlarge);
	  }else
	  {
	    testFlatBottomTriangle(p2c, pm, p3c, idRegion, complementaryPoints, myPoints, maxX, maxY, minX, minY, numberPixelEnlarge);
	    ok = false;
	  }
      return ok;
    }
}

void drawHull(Board2D &aBoard, vector<Point> &hull)
{
	if (hull.size() > 1)
	{
		for (int j = 0; j < hull.size() - 1; ++j)
		{
		    Point p1 = hull[j],p2 = hull[j + 1];
		    aBoard.drawLine(p1[0], p1[1], p2[0], p2[1]);
		}
		Point p1 = hull[hull.size() - 1],p2 = hull[0];
		aBoard.drawLine(p1[0], p1[1], p2[0], p2[1]);
	}
}

void drawBoundery(Board2D &aBoard, vector<Point> &boundery)
{
	if (boundery.size() > 1)
	{
		aBoard.setPenColor(DGtal::Color::White);

		for (int j = 0; j < boundery.size() - 1; ++j)
		{
		    Point p1 = boundery[j],p2 = boundery[j + 1];
		    aBoard.drawLine(p1[0], p1[1], p2[0], p2[1]);
		}
		Point p1 = boundery[boundery.size() - 1],p2 = boundery[0];
		aBoard.drawLine(p1[0], p1[1], p2[0], p2[1]);
	}
}

void drawPolygon(Board2D &aBoard, vector<Point> &boundery)
{
	if (boundery.size() > 1)
	{
		aBoard.setPenColor(DGtal::Color::Blue);

		for (int j = 0; j < boundery.size() - 1; ++j)
		{
		    Point p1 = boundery[j],p2 = boundery[j + 1];
		    aBoard.drawLine(p1[0], p1[1], p2[0], p2[1]);
		}
		Point p1 = boundery[boundery.size() - 1],p2 = boundery[0];
		aBoard.drawLine(p1[0], p1[1], p2[0], p2[1]);
	}
}

void getConvexHull(Region &component)
{
	vector<Point> points = component.points;

	int nbPoint = points.size(), k = 0;

	if (nbPoint == 1) 
	{
		component.hull = points;
		return;
	}

	vector<Point> hull( 2 * nbPoint );

	sort(points.begin(), points.end());

	for (int i = 0; i < nbPoint; ++i) {
	    while (k >= 2 && cross(hull[k-2], hull[k-1], points[i]) <= 0) 
	    {
	    	k--;
	    }
    	hull[k++] = points[i];
  	}

  	component.indexBreakPointHull = k;

  	for (int i = nbPoint - 2, t = k+1; i >= 0; i--) {
	    while (k >= t && cross(hull[k-2], hull[k-1], points[i]) <= 0) 
	    {
	    	k--;
	    }
    	hull[k++] = points[i];
  	}

  	hull.resize(k-1);

  	component.hull = hull;
}

double cross(const Point &p0, const Point &p1, const Point &p2)
{
	return (p1[0] - p0[0]) * (p2[1] - p0[1]) - (p1[1] - p0[1]) * (p2[0] - p0[0]);
}

// ERROR assign label every myPoint => change current label - fixed
void mergeEquivalentRegion(vector<vector<int>> &equivalentRegions, int sizeEquivalentRegion, vector<Region> &regions, 
	vector<vector<MyPoint>> &myPoints, Domain domain, int &levelRegion, int countRegion, int numberPixelEnlarge)
{
	int count =  regions.size(), firstCount = regions.size();
	for (int i = 0; i < sizeEquivalentRegion; ++i)
	{
	    if (equivalentRegions.at(i).size() > 0)
	    {
	    	Region region;
	    	region.id = count;
	    	region.level = levelRegion;
	    	region.color = Color( rand() % 192 + 64, rand() % 192 + 64, rand() % 192 + 64, 255);

		    for (int j = domain.lowerBound()[0]; j <= domain.upperBound()[0]; ++j)
		    {
		        for (int k = domain.lowerBound()[1]; k <= domain.upperBound()[1]; ++k)
		        {
			        //if (myPoints[j + numberPixelEnlarge][k + numberPixelEnlarge].isComponent)
			        //{
			            if (findIdEquivalentRegion(equivalentRegions, myPoints[j + numberPixelEnlarge][k + numberPixelEnlarge].label, countRegion) == i)
			            {
			            	region.points.push_back(myPoints[j + numberPixelEnlarge][k + numberPixelEnlarge].coordinate);
			            }
			        //}
		        }
		    }

			getConvexHull(region);
			calculateComplementaryInformation(region.hull, region.perimeter, region.area);

		    regions.push_back(region);
		    count++;      
	    }
	} 

	for (int i = firstCount; i < regions.size(); ++i)
	{
		vector<Point> p = regions.at(i).points;
		for(int j = 0; j < p.size(); j++)
		{
			myPoints[p.at(j)[0] + numberPixelEnlarge][p.at(j)[1] + numberPixelEnlarge].label = regions.at(i).id;
		}
	}
	levelRegion++;
}

void formWellComposed(Image &image, int thresholdImage)
{
	Image::Domain domain = image.domain();
	int widthImg = domain.upperBound()[0];
    int heightImg = domain.upperBound()[1];

	for (int i = 0; i < widthImg; ++i)
	{
		for (int j = 0; j < heightImg; ++j)
	    {
	      Point p1(i, j); int value1 = (int)image(p1);
	      Point p2(i + 1, j); int value2 = (int)image(p2);
	      Point p3(i + 1, j + 1); int value3 = (int)image(p3);
	      Point p4(i, j + 1); int value4 = (int)image(p4);

	      /*
	        | p1 | p2 |
	        -----------
	        | p4 | p3 |
	      */

	      if ( ( ( value1 >= thresholdImage ) && ( value2 < thresholdImage ) && ( value3 >= thresholdImage ) && ( value4 < thresholdImage ) ) ||
	            ( ( value1 < thresholdImage ) && ( value2 >= thresholdImage ) && ( value3 < thresholdImage ) && ( value4 >= thresholdImage ) ) )
	      {
	        image.setValue(p1, thresholdImage + 1);
	        image.setValue(p2, thresholdImage + 1);
	        image.setValue(p3, thresholdImage + 1);
	        image.setValue(p4, thresholdImage + 1);
	      }
	    }
	}
}

void coloringConnectedComponent(Board2D &aBoard, Region region)
{
	vector<Point> points = region.points;
	for (int i = 0; i < points.size(); ++i)
	{      
	    aBoard << CustomStyle( points[i].className(), 
	            new CustomPen( region.color, region.color, 1.0, 
	                                    Board2D::Shape::SolidStyle,
	                                    Board2D::Shape::RoundCap,
	                                    Board2D::Shape::RoundJoin )) << points[i];         
	}
}

void initTrees(vector<Region> &regions, vector<Tree> &trees)
{
	for (int i = 0; i < regions.size(); ++i)
	{
		Tree tree;
		tree.idRootRegion = regions.at(i).id;

		Node node;
		node.idRegion = regions.at(i).id;

		tree.nodes.push_back(node);

		trees.push_back(tree);
	}
}

void generateColorImage(Board2D &aBoard, vector<Region> &regions)
{
	for (int i = 0; i < regions.size(); ++i)
	{
		coloringConnectedComponent(aBoard, regions[i]);
	}

	for (int i = 0; i < regions.size(); ++i)
	{
		if (regions.at(i).isHConvex)
		{
		    aBoard.setPenColor(DGtal::Color::Blue);
		    drawHull(aBoard, regions.at(i).hull);
		}
		else
		{
		    aBoard.setPenColor(DGtal::Color::Red);
		    drawHull(aBoard, regions.at(i).hull);
		}
	}
}


void generateGraphFile(vector<Tree> &trees, vector<Region> &regions)
{
	ofstream outfile;
  	outfile.open("result/graph.dot");
  	outfile << "digraph G {\n\t graph [bgcolor=white, splines=true];\n\t edge [color=black, arrowsize=2];\n\t node [style=filled, shape=polygon, sides=6]; \n";

  	for(int i = 0; i < trees.size(); i++)
  	{
  		vector<Node> nodesOfTree = trees.at(i).nodes;
  		for(int j = 0; j < nodesOfTree.size(); j++)
  		{
  			Node currentNode = nodesOfTree.at(j);
  			vector<Node> subnodes = currentNode.subnodes;
  			for(int k = 0; k < subnodes.size(); k++)
  			{
  				outfile << "\t" << currentNode.idRegion << " -> " << subnodes.at(k).idRegion << ";\n";
  			}
  		}
  	}

  	for(int i = 0; i < regions.size(); i++)
	{
		if (!regions.at(i).isSubComponent)
		{
			Color colorRegion = regions.at(i).color;

		    stringstream ssR;
		    ssR << hex << (int)colorRegion.red(); 
		    string r ( ssR.str() );

		    stringstream ssG;
		    ssG << hex << (int)colorRegion.green(); 
		    string g ( ssG.str() );

		    stringstream ssB;
		    ssB << hex << (int)colorRegion.blue(); 
		    string b ( ssB.str() );

		    stringstream ssA;
		    ssA << hex << (int)colorRegion.alpha(); 
		    string a ( ssA.str() );
		    
		    outfile << "\t"<<i<< " [color = \" #"<< r << g << b << a <<" \" ] ;\n";
		}
	}

	outfile << "} \n";
	outfile.close();
}

void generatePolygonFile(vector<Region> &regions)
{
	ofstream outfile;
  	outfile.open("result/polygon.txt");

  	for(int i = 0; i < regions.size(); i++)
	{
		outfile << regions.at(i).id << " ";
		vector<Point> polygon = regions.at(i).polygon;

		for(int j = 0; j < polygon.size() - 1; j++)
		{
			outfile << polygon.at(j)[0] << " " << polygon.at(j)[1] << " ";
		}

		outfile << polygon.at(polygon.size() - 1)[0] << " " << polygon.at(polygon.size() - 1)[1] << endl;
	}

	outfile.close();
}